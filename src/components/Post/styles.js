import {Dimensions, FlexBox, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  img: {
    aspectRatio: 3 /3,
    width: '100%',
    resizeMode: 'cover',
    borderRadius: 50,
    marginTop: 20,
    marginLeft: -10,
  },
  imgContainer: {paddingLeft: 20},
  titleContainer: {
    margin: 10,
    flexDirection: 'row',
    alignContent: 'space-around',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    backgroundColor: '#00171f',
    borderWidth: 1,
    borderRadius: 20,
    opacity: 0.7,
  },
  issueNoContainer: {
    width: '100%',
    flexDirection: 'row',
    alignContent: 'space-around',
    justifyContent: 'space-evenly',
    alignItems: 'center',
    backgroundColor: '#474a4d',
    opacity: 0.5,
    borderRadius: 10,
  },
  headerContainer: {
    color: '#e71d34',
  },
  container: {
    margin: 30,
    alignItems: 'center',
    marginTop: 50,
  },
  headerContainer: {
    flexDirection: 'row',
  },
  comicTitle: {
    fontSize: 30,
    margin: 10,
   // fontWeight: 'bold',
    color: '#e71d34',
    fontFamily: 'Bangers',
  },
  priceTitle: {
    margin: 2,
    fontSize: 17,
    paddingLeft: 10,
    color: '#d9e3dd',
    fontFamily: 'Bangers',
  },
  issueNoTitle: {
    margin: 2,
    fontSize: 15,
    paddingLeft: 10,
    justifyContent: 'center',
    color: '#d9e3dd',
    //fontWeight: 'bold',
    fontFamily: 'Bangers',
  },
});

export default styles;
