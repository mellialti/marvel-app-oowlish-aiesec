import {Image, Pressable, Text, View} from 'react-native';

import Bangers from '../../../assets/fonts/Bangers.ttf';
import React from 'react';
import reactNativeElements from 'react-native-elements';
import styles from './styles';

const Post = props => {
  const post = props.post;
  return (
   
    <View styles={styles.container}>
      {/*Comics Image*/}
      <View style={styles.imgContainer}>
        <Image style={styles.img}  source={{uri: post.thumbnail.path+'.'+post.thumbnail.extension}} /> 
      </View>

      {/*Comic Name*/}
      <View style={styles.headerContainer}>
        <View style={styles.titleContainer}>
          <Text style={styles.comicTitle}>{post.title}</Text>
        </View>
        <View style={styles.iconContainer}></View>
      </View>
      {/*Comics Price*/}
      <Text style={styles.priceTitle}> Price: {post.prices[0].price} $ </Text>
      {/*Comics Issue No*/}
      <View style={styles.issueNoContainer}>
        <Text style={styles.issueNoTitle}>Issue No: {post.issueNumber}</Text>
      </View>
    </View>
  );
};

export default Post;
