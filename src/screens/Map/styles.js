import {Dimensions, FlexBox, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  image: {
    height: '100%',
    width: '100%',
    resizeMode: 'cover',
  },
  container: {flex: 1, justifyContent: 'center', paddingTop: 380},
  headerText: {
    fontSize: 65,
    margin: 10,
    backgroundColor: '#1d3658',
    opacity: 0.8,
    borderRadius: 30,
    color: '#f2faef',
    fontFamily: 'Bangers',
    marginBottom: 0,
    paddingLeft: 40,
  },
  bodyText: {
    opacity: 0.8,
    marginRight: 100,
    fontSize: 25,
    color: '#e6ebe0',
    backgroundColor: '#ef253c',
    borderRadius: 30,
    fontFamily: 'Bangers',
    paddingLeft: 20,
    paddingTop: 2,
    paddingBottom: 2,
  },
  bodyContainer: {flex: 1, paddingLeft: 20, },
  headerContainer: {flex: 1},
});

export default styles;
