import {
  Container,
  Header,
  ImageBackground,
  Pressable,
  Text,
  View,
} from 'react-native';

import {Icon} from 'react-native-elements';
import React from 'react';
import styles from './styles';

const img = '../../../assets/images/backgroundImageMain.jpeg';
const img2 = {
  uri: 'https://i.imgflip.com/c0up7.jpg',
};

const MapScreen = props => {
  return (
    <View>
      <ImageBackground source={img2} style={styles.image}>
        <View style={styles.container}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerText}> Marvel Map</Text>
          </View>
          <View style={styles.bodyContainer}>
            <Text style={styles.bodyText}> Dude Can't Find Those Stores...Maybe Soon?</Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

export default MapScreen;
