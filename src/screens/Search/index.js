import {
  ActivityIndicator,
  FlatList,
  Header,
  ImageBackground,
  SafeAreaView,
  Text,
  TextInput,
  View,
} from 'react-native';
import {Icon, SearchBar} from 'react-native-elements';
import React, {Component, useState} from 'react';

import Post from '../../components/Post';
import feed from '../../../assets/data/comicsfeed';
import styles from './styles';

let helperArray = feed;
const image = {
  uri:
    'https://cutewallpaper.org/21/marvel-hd-wallpapers/10-of-High-Resolution-Marvel-Cellphone-Wallpapers-HD.jpg',
};
class SearchResultsScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {search: '', isLoading: true};
    this.arrayholder = feed;
  }
  componentDidMount() {
    return;
    this.setState(
      {
        isLoading: false,
        dataSource: feed,
      },
      function () {
        this.arrayholder = feed;
      },
    );
  }
  search = text => {
    console.log('searching...');
  };
  clear = () => {
    this.search.clear();
  };
  SearchFilterFunction(text) {
    const newData = this.arrayholder.filter(function (item) {
      const itemData = item.title ? item.title.toUpperCase() : ''.toUpperCase();
      const textData = text.toUpperCase();
      return itemData.indexOf(textData) > -1;
    });
    this.setState({dataSource: newData, search: text});
  }

  render() {
    return (
      <SafeAreaView >
        <View styles={styles.container}>
          <ImageBackground source={image} style={styles.bgImg}>
            <SearchBar
              placeholder="Whose Comic...?"
              onChangeText={text => this.SearchFilterFunction(text)}
              onClear={text => this.SearchFilterFunction('')}
              value={this.state.search}
              hideBackground={true}
              searchBarStyle="minimal"
              round
            />
            <FlatList
              data={this.state.dataSource}
              renderItem={({item}) => <Post post={item} />}
              enableEmptySections={true}
            />
          </ImageBackground>
        </View>
      </SafeAreaView>
    );
  }
}

/* const SearchResultsScreen = props => {
  const [inputText, setInputText] = useState('');
  console.log(inputText);
  return (
    <View styles={styles.container}>
      
      <SearchBar
        placeholder="Comics Of Your Favorite Hero..."
        value={inputText}
        onChangeText={setInputText}
        buttonStyle
      />
      <FlatList data={feed} renderItem={({item}) => <Post post={item} />} />
    </View>
  );
}; */

export default SearchResultsScreen;
