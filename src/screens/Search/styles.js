import {Dimensions, FlexBox, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  textInput: {
    fontSize: 20,
  },
  container: {
    margin: 20,
  },
  bgImg: {
    width: '100%',
    height: '100%',
  },
});

export default styles;
