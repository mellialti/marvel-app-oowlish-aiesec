import {
  Container,
  Header,
  ImageBackground,
  Pressable,
  Text,
  View,
} from 'react-native';

import Bangers from '../../../assets/fonts/Bangers.ttf';
import {Icon} from 'react-native-elements';
import React from 'react';
import styles from './styles';

const img = '../../../assets/images/backgroundImageMain.jpeg';
const img2 = {
  uri:
    'https://images.unsplash.com/photo-1596727147705-61a532a659bd?ixid=MXwxMjA3fDB8MHxzZWFyY2h8MXx8bWFydmVsfGVufDB8fDB8&ixlib=rb-1.2.1&w=1000&q=80',
};

const DetailsScreen = props => {
  return (
    <View style={styles.container}>
      <ImageBackground source={img2} style={styles.image}>
        <View style={styles.textContainer}>
          <View style={styles.headerContainer}>
            <Text style={styles.headerText}>MARVEL COMICS</Text>
          </View>
          <View style={styles.bodyContainer}>
            <Text style={styles.bodyText}>
              Where you search for your favorite hero's comics!
            </Text>
          </View>
        </View>
      </ImageBackground>
    </View>
  );
};

export default DetailsScreen;
