import {Dimensions, StyleSheet} from 'react-native';

const styles = StyleSheet.create({
  image: {
    height: '100%',
    width: '100%',
    resizeMode: 'cover',
  },
  headerText: {
    fontSize: 50,
    backgroundColor: '#1c2540',
    color: '#fff',
    opacity: 0.5,
    borderWidth: 1,
    borderRadius: 50,
    fontFamily: 'Bangers',
    paddingLeft: 40,
  },
  textContainer: {
    padding: 100,
    alignContent: 'center',
    justifyContent: 'center',
  },
  headerContainer: {paddingTop: 150},
  bodyContainer: {paddingTop: 30},
  bodyText: {
    color: '#fcfffa',
    fontSize: 20,
    backgroundColor: '#1c2540',
    opacity: 0.7,
    fontFamily: 'Bangers',
    paddingHorizontal: 10,
    padding: 20,
  },
});

export default styles;
