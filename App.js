/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */
import {
  ImageBackground,
  SafeAreaView,
  ScrollView,
  StatusBar,
  StyleSheet,
  Text,
  View,
  useColorScheme,
} from 'react-native';
import React, {Component} from 'react';

import DetailsScreen from './src/screens/Details';
import {Icon} from 'react-native-elements';
import MapScreen from './src/screens/Map';
import type {Node} from 'react';
import Router from './src/navigation/router';
import SearchResultsScreen from './src/screens/Search';
import {createMaterialBottomTabNavigator} from 'react-navigation-material-bottom-tabs';
import feed from './assets/data/comicsfeed';
import styles from './src/components/Post/styles';

//const Tab = createMaterialBottomTabNavigator();
const imageBG = {
  uri:
    'https://cutewallpaper.org/21/marvel-hd-wallpapers/10-of-High-Resolution-Marvel-Cellphone-Wallpapers-HD.jpg',
};

class HomeScreen extends Component {
  render() {
    return (
      <View>
        <SearchResultsScreen />
      </View>
    );
  }
}
class DetailScreen extends Component {
  render() {
    return (
      <View>
        <DetailsScreen />
      </View>
    );
  }
}
class MapsScreen extends Component {
  render() {
    return (
      <View>
        <MapScreen />
      </View>
    );
  }
}

export default createMaterialBottomTabNavigator(
  {
    Comics: {
      screen: SearchResultsScreen,
      navigationOptions: {
        tabBarLabel: 'Comics',
        tabBarIcon: <Icon name="search" color="red" size={24} />,
      },
    },
    Details: {
      screen: DetailsScreen,
      navigationOptions: {
        tabBarLabel: 'Details',
        tabBarIcon: <Icon name="home" color="red" size={24} />,
      },
    },
    Map: {
      screen: MapScreen,
      navigationOptions: {
        tabBarLabel: 'Map',
        tabBarIcon: <Icon name="map" color="red" size={24} />,
      },
    },
  },
  {
    initialRouteName: 'Comics',
    // order:['Comics','Details'],
    activeTintColor: 'blue',
  },
);

const stylesApp = StyleSheet.create({
  img: {
    resizeMode: 'cover',
  },
  scrollView: {
    marginHorizontal: 10,
  },
});
